package br.com.prave.controller;

import br.com.prave.model.Funcionario;
import br.com.prave.repository.FuncionarioRepository;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class FuncionarioController {

    private FuncionarioRepository funcionarioRepository;

    @RequestMapping(value = "/funcionarios", method = RequestMethod.GET)
    public List<Funcionario> findAll() {
        return funcionarioRepository.findAll();
    }

    @RequestMapping(value = "/funcionarios/create", method = RequestMethod.POST)
    public List<Funcionario> create(@RequestBody Funcionario funcionario) {
        funcionarioRepository.save(funcionario);
        return funcionarioRepository.findAll();
    }
}
