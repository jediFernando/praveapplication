package br.com.prave.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.security.Principal;

@Controller
public class ViewController {

    @GetMapping("")
    public static ModelAndView exibeInicio() {
        ModelAndView modelAndView = new ModelAndView("/index");
        return modelAndView;
    }

    @GetMapping("/login")
    public ModelAndView exibeLogin() {
        ModelAndView modelAndView = new ModelAndView("/login");
        return modelAndView;
    }
}
