package br.com.prave.controller;

import br.com.prave.model.Empresa;
import br.com.prave.model.Funcionario;
import br.com.prave.repository.EmpresaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class EmpresaController {
    private EmpresaRepository empresaRepository;

    @Autowired
    public EmpresaController(EmpresaRepository empresaRepository) {
        this.empresaRepository = empresaRepository;
    }

    @RequestMapping(value = "/empresas", method = RequestMethod.GET)
    public List<Empresa> findAll() {
        return empresaRepository.findAll();
    }

    // TODO método bom para filtro de estoque e/ou venda. Não faz sentido aqui, alterar depois.
//    @RequestMapping(value = "/empresas/filtra/{documento}", method = RequestMethod.GET)
//    public List<Empresa> getAffordable(@PathVariable int documento) {
//        return empresaRepository.findbyDocumentoLessThan(documento);
//    }

    @RequestMapping(value = "/empresas/create", method = RequestMethod.POST)
    public List<Empresa> create(@RequestBody Empresa empresa) {
        empresaRepository.save(empresa);
        return empresaRepository.findAll();
    }

    @RequestMapping(value = "/empresas/delete/{id}", method = RequestMethod.GET)
    public List<Empresa> delete (@PathVariable long id) {
        empresaRepository.delete(id);
        return empresaRepository.findAll();
    }
}
