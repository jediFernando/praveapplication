package br.com.prave;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PraveApplication {

    public static void main(String[] args) {
        SpringApplication.run(PraveApplication.class, args);
    }
}