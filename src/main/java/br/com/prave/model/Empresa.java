package br.com.prave.model;

import org.springframework.data.jpa.domain.AbstractPersistable;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
public class Empresa extends AbstractPersistable<Long> {

    private static final long serialVersionUID = -7302800336276816169L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long id;

    private String name;
    private int document;
    private String address;
    private String email;

    @OneToMany(targetEntity = Funcionario.class, mappedBy = "empresa", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Set<Funcionario> funcionarios;

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDocument() {
        return document;
    }

    public void setDocument(int document) {
        this.document = document;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setId(long id) {
        this.id = id;
    }
}
